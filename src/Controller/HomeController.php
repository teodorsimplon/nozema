<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Persistence\ManagerRegistry;
use App\Entity\Products;
use App\Entity\Users;
use App\Form\SearchProductsType;
use Doctrine\ORM\EntityManagerInterface;

class HomeController extends AbstractController
{
    #[Route('/', name: 'app_home')]
    public function index(Request $request, ManagerRegistry $doctrine, EntityManagerInterface $entityManager): Response
    {

        $products = $doctrine->getRepository(Products::class)->findAll();

        $form = $this->createForm(SearchProductsType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            
            $researchedProducts = $doctrine->getRepository(Products::class)->findBy($data);
            
            return $this->redirectToRoute('app_home');
        }
       
                return $this->render('home/index.html.twig', [
                    'form' => $form->createView(),
                    'products' => $products,
                    'data' => $this

                ]);
            }
        }

