<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\ProductsRepository;
use App\Entity\Products;
use App\Form\ProductsType;

class ProductsPageController extends AbstractController
{
    #[Route('/prod', name: 'app_products_page')]
    public function index(ProductsRepository $productsRepository): Response
    {
        return $this->render('products_page/index.html.twig', [
            'products' => $productsRepository->findAll(),
        ]);
    }
}
