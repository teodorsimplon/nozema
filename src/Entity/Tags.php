<?php

namespace App\Entity;

use App\Repository\TagsRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: TagsRepository::class)]
class Tags
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private $name;

    #[ORM\ManyToMany(targetEntity: Products::class)]
    private $productTag;

    public function __construct()
    {
        $this->productTag = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int, Products>
     */
    public function getProductTag(): Collection
    {
        return $this->productTag;
    }

    public function addProductTag(Products $productTag): self
    {
        if (!$this->productTag->contains($productTag)) {
            $this->productTag[] = $productTag;
        }

        return $this;
    }

    public function removeProductTag(Products $productTag): self
    {
        $this->productTag->removeElement($productTag);

        return $this;
    }
}
